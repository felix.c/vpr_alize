cp -f ../../SPro_5.0/sfbcep bin/
cp -f ../../LIA_RAL_3.0/bin/NormFeat bin/
cp -f ../../LIA_RAL_3.0/bin/EnergyDetector bin/
cp -f ../../LIA_RAL_3.0/bin/ComputeTest bin/
cp -f ../../LIA_RAL_3.0/bin/ComputeNorm bin/
cp -f ../../LIA_RAL_3.0/bin/TrainTarget bin/
cp -f ../../LIA_RAL_3.0/bin/TrainWorld bin/

rm -rf gmm/;mkdir gmm
rm -rf log/;mkdir log
rm -rf data/lbl/;mkdir data/lbl
rm -rf data/prm/;mkdir data/prm
